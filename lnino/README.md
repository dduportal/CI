

# API de généralisation cartographique

La généralisation est un principe cartographique servant à la simplification et l’adaptation du tracé d’un contour cartographique à l’échelle de restitution de la carte. Dans le cadre de ce projet, le généralisation est dite structurale, elle ne prend pas en compte les changements d’échelle.

définition : http://geoconfluences.ens-lyon.fr/glossaire/generalisation-carto

Cette API va permettre la généralisation de :
- Polylignes (algorithme de Douglas-Peucker)
- Points (création d'une grille régulière et points moyens)
- Polygones (seulement des angles droits)
  * Maven Build
  * JUnit-tested


## Importation de l'API

Le package de l'API se situe dans le dossier target du projet

```
	<dependency>
  	    <groupId>eu.ensg.projetperso2</groupId>
  	    <artifactId>lnino</artifactId>
  	    <version>0.0.1-SNAPSHOT</version>
  	    <scope>system</scope>
  	    <systemPath>/target/lnino-0.0.1-SNAPSHOT.jar</systemPath>
	</dependency>
```

## Polylignes

La simplification est réalisée à partir de l'algorithme de Douglas-Peucker.
(https://fr.wikipedia.org/wiki/Algorithme_de_Douglas-Peucker)

Afin de généraliser la géometrie, il faut:
```java
  //Créer un objet
  GeneralisationLineString lines = new GeneralisationLineString(double epsilon);
  double epsilon = ... //Seuil comparé avec la distance au point distant afin de déterminer les nœuds à supprimer

  //Ouvrir un shapefile
  line.openShapeFile();

  //Simplifier la géométrie et la stocker dans un nouveau shapefile
  line.createNewShapeFile();
```

## Points

La simplification est réalisée en calculant la moyenne des coordonnées des points situés dans chaque case de la grille régulière.

Afin de généraliser la géometrie, il faut:
```java
  //Créer un objet
  GeneralisationPoints points = new GeneralisationPoints(int pas);
  int pas = ... //Pas permettant la détermination du nombre d'enveloppes constituant la grille régulière

  //Ouvrir un shapefile
  points.openShapeFile();

  //Simplifier la géométrie et la stocker dans un nouveau shapefile
  points.createNewShapeFile();
```

## Polygones

La simplification est réalisée en transformant les polygones en rectangles.

Afin de généraliser la géometrie, il faut:
```java
  //Créer un objet
  GeneralisationPolygons polygons = new GeneralisationPolygons();

  //Ouvrir un shapefile
  polygons.openShapeFile();

  //Simplifier la géométrie et la stocker dans un nouveau shapefile
  polygons.createNewShapeFile();
```


