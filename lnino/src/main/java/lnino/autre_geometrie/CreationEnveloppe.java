package lnino.autre_geometrie;

import com.vividsolutions.jts.geom.Envelope;

public interface CreationEnveloppe {
	
	public Envelope[][] createEnvelopes(double minX, double maxX, double minY, double maxY);

}
