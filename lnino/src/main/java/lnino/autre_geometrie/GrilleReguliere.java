package lnino.autre_geometrie;

import com.vividsolutions.jts.geom.Envelope;

import lnino.exception.PasException;

/**
 * Classe permettant la création de grille régulière
 * 
 * @author lnino
 * @version 1.0
 *
 */
public class GrilleReguliere implements CreationEnveloppe{
	
	int nbEnv;
	
	/**
	 * Constructeur de la classe GrilleReguliere
	 * 
	 * @param nbEnv 
	 * 			 correspond au pas permettant la création de la grille
	 * @throws PasException
	 */
	public GrilleReguliere(int nbEnv) throws PasException {
		if(nbEnv <= 1){
			throw new PasException();
		}
		this.nbEnv = nbEnv;
	}
	
	/**
	 * Retourne la valeur nbEnv
	 * 		
	 * @return int - nbEnv
	 * 
	 */
	public int getNbEnv() {
		return nbEnv;
	}
	
	/**
	 * Attribue un pas à une GénéralisationLineString
	 * 
	 * @param int - nbEnv
	 */
	public void setNbEnv(int nbEnv) {
		this.nbEnv = nbEnv;
	}
	
	/**
	 * Cette méthode permet de créer une grille régulière à partir d'une enveloppe
	 * @param minX
	 * 		x minimal de l'emprise du shapefile
	 * @param maxX
	 * 		x maximal de l'emprise du shapefile
	 * @param minY
	 * 		y minimal de l'emprise du shapefile
	 * @param maxY
	 * 		y maximal de l'emprise du shapefile
	 * @return un tableau d'enveloppe représentant la grille 
	 */
	@Override
	public Envelope[][] createEnvelopes(double minX, double maxX, double minY, double maxY) {
			
		int lX = (int) (maxX - minX)/this.nbEnv + 1;
		int lY = (int) (maxY - minY)/this.nbEnv + 1;
		
		Envelope[][] envTab = new Envelope[lX][lY];
		
		for(int i = 0; i < lX; i++){
			for(int j = 0; j < lY; j++)
			{
				envTab[i][j] = new Envelope(minX+i*this.nbEnv, minX+(j+1)*this.nbEnv, minY+j*this.nbEnv, minY+(i+1)*this.nbEnv);		
			}
		}
		return envTab;
	}
	

}
