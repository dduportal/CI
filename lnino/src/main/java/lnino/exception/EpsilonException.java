package lnino.exception;

public class EpsilonException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EpsilonException(){
		System.out.println("Epsilon doit être supérieur à 0");
	}

}
