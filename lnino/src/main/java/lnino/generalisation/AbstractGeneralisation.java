package lnino.generalisation;

import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

import lnino.exception.PasException;

public abstract class AbstractGeneralisation {

	public abstract Coordinate[] fonctionDouglasPeucker(Coordinate[] points, int startIndex, int endIndex);
	
	public abstract LineString createLineString(Coordinate[] pts);
	
	public abstract List<Coordinate> genPoint(List<Coordinate> pts, double minX, double maxX, double minY, double maxY) throws PasException;
	
	public abstract Point createPoint(Coordinate coords);
	
	public abstract Coordinate[] genePolygon(Polygon p);
	
	public abstract Polygon createPolygone(Coordinate[] coords);
}
