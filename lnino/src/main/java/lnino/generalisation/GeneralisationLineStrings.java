package lnino.generalisation;


import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

import lnino.exception.EpsilonException;
import lnino.exception.PasException;
import lnino.shapefile.GestionShapeFile;

/**
 * Classe permettant la simplification d'une ligne
 * 
 * @author lnino
 * @version 1.0
 */
public class GeneralisationLineStrings extends AbstractGeneralisation implements GestionShapeFile{

	SimpleFeatureSource featureSource;
	
	private double epsilon;
	
	/**
	 * Constructeur de la classe GeneralisationLineString
	 * 
	 * @param epsilon {@link Double}
	 * 			 correspond à la distance maximale à partir de laquelle les points seront supprimés
	 * @throws EpsilonException
	 */
	public GeneralisationLineStrings(double epsilon) throws EpsilonException{
		if(epsilon <= 0){
			throw new EpsilonException();
		}
		this.epsilon = epsilon;
	}
	
	/**
	 * Retourne la valeur d'epsilon	
	 * 		
	 * @return {@link Double} epsilon
	 * 
	 */
	public double getEpsilon() {
		return epsilon;
	}
	
	/**
	 * Attribue une valeur d'epsilon à une GénéralisationLineString
	 * 
	 * @param epsilon {@link Double}
	 */
	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}
	
	/**
	 * Cette méthode permet de calculer la distance entre chaque point situés entre les extrémités
	 * 		de la polyligne et le segment formé à partir des deux extrémités de cette même polyligne
	 * 
	 * @param coords {@link Coordinate[]}
	 * 		la liste des coordonnées formant la polyligne
	 * @param startIndex {@link Integer}
	 * 		le premier index de la liste 
	 * @param endIndex {@link Integer}
	 * 		le dernier index de la liste (taille de la liste - 1)
	 * @param c {@link Coordinate}
	 * 		le point à comparer avec le segment des extrémités
	 * @return {@link Double} distance calculée
	 */
	public double distance(Coordinate[] coords, int startIndex, int endIndex, Coordinate c){
		double dx = coords[endIndex].x - coords[startIndex].x;
		double dy = coords[endIndex].y - coords[startIndex].y;
		double m1 = dx * (c.y - coords[startIndex].y);
		double m2 = (c.x - coords[startIndex].x) * dy;
		double d = Math.abs(m1 - m2);
		double n = Math.sqrt(Math.pow(dx,2) + Math.pow(dy,2));
		return d / n;
	}
	
	/**
	 * Cette méthode implémente la fonction de Douglas Peucker
	 * 
	 * @param points {@link Coordinate[]}
	 * 		liste des coordonnées de la polyligne à simplifier
	 * @param startIndex {@link Integer}
	 * 		le premier index de la liste
	 * @param endIndex {@link Integer}
	 * 		le dernier index de la liste (taille de la liste - 1)
	 * @return {@link Coordinate[]}
	 * 		une liste de coordonnées contenant les points n'ayant pas été supprimé 
	 */
	public Coordinate[] fonctionDouglasPeucker(Coordinate[] points, int startIndex, int endIndex){
		double dmax = 0;
		int index = 0;
		for(int i = startIndex + 1; i < endIndex; i++){
			double distance = this.distance(points, startIndex, endIndex, points[i]);
			if (distance > dmax){
				index = i;
				dmax = distance;
			}
		}
						
		if(dmax >= epsilon){
			Coordinate[] resultRecursif1 = this.fonctionDouglasPeucker(points, startIndex, index);
			Coordinate[] resultRecursif2 = this.fonctionDouglasPeucker(points, index, endIndex);
			Coordinate[] resultat = new Coordinate[(resultRecursif1.length - 1) + resultRecursif2.length];
			System.arraycopy(resultRecursif1, 0, resultat, 0, resultRecursif1.length - 1);
			System.arraycopy(resultRecursif2, 0, resultat, resultRecursif1.length - 1, resultRecursif2.length);			
			return resultat;			
		}
		else {
         return new Coordinate[] {points[startIndex], points[endIndex]};
		}
	}
	/**
	 * Cette méthode permet de construire une polyligne à partir d'une liste de coordonnées
	 * 
	 * @param pts {@link Coordinate[]}
	 * @return {@link LineString} 
	 */
	public LineString createLineString(Coordinate[] pts){
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		Coordinate[] coords = fonctionDouglasPeucker(pts, 0, pts.length - 1);
		return geometryFactory.createLineString(coords);
	}

	@Override
	public void createNewShapeFile() throws SchemaException, IOException {

		File newShape = new File("linestring.shp");
		SimpleFeatureType type = DataUtilities.createType("linestring","the_geom:LineString:srid=2154");
		
		ShapefileDataStoreFactory factory = new ShapefileDataStoreFactory();
		
		Map<String, Serializable> newMap = new HashMap<String, Serializable>();
		
		newMap.put("url", newShape.toURI().toURL());
		
		ShapefileDataStore newStore = (ShapefileDataStore) factory.createNewDataStore(newMap);
		
		newStore.createSchema(type);
		
		List<SimpleFeature> features = new ArrayList<SimpleFeature>();
			
		SimpleFeatureBuilder featureBuild = new SimpleFeatureBuilder(type);
		
		Transaction transaction = new DefaultTransaction("create");
		
		SimpleFeatureCollection featCollection = this.featureSource.getFeatures();
		SimpleFeatureIterator iterator = featCollection.features();
		
		while(iterator.hasNext()) {
			
			SimpleFeature feature = iterator.next();
			Geometry geom = (Geometry) feature.getAttribute(0);
			MultiLineString multi = (MultiLineString) geom;
			Coordinate[] points = multi.getCoordinates();
		 		 
		 featureBuild.add(this.createLineString(points));
		 SimpleFeature newFeature = featureBuild.buildFeature(null);
		 features.add(newFeature);
		}
		
		 String typeName = newStore.getTypeNames()[0];
		 SimpleFeatureSource featureSource = newStore.getFeatureSource(typeName);
		 featureSource.getSchema();
		 if (featureSource instanceof SimpleFeatureStore) {
             SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
    		 SimpleFeatureCollection newCollect = new ListFeatureCollection(type, features);
             featureStore.setTransaction(transaction);
             try {
                 featureStore.addFeatures(newCollect);
                 transaction.commit();
             } catch (Exception problem) {
                 problem.printStackTrace();
                 transaction.rollback();
             } 
         }
		
		transaction.close();
		newStore.dispose();	
	}

	@Override
	public void openShapeFile() throws IOException {
		File file = JFileDataStoreChooser.showOpenFile("shp", null);
		if(file == null){
			return;
		}
		
		FileDataStore store = FileDataStoreFinder.getDataStore(file);
		this.featureSource = store.getFeatureSource();
        MapContent map = new MapContent();
		
		Style style = SLD.createSimpleStyle(featureSource.getSchema());
		Layer layer = new FeatureLayer(featureSource, style);
		map.addLayer(layer);
		
		JMapFrame.showMap(map);
	}

	@Override
	public List<Coordinate> genPoint(List<Coordinate> pts, double minX, double maxX, double minY, double maxY)
			throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Point createPoint(Coordinate coords) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Coordinate[] genePolygon(Polygon p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Polygon createPolygone(Coordinate[] coords) {
		// TODO Auto-generated method stub
		return null;
	}		
}

