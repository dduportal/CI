package lnino.shapefile;

import java.io.File;
import java.io.IOException;

import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.feature.SchemaException;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;

/**
 * Classe permettant l'ouverture de shapefile
 * 
 * @author lnino
 * @version 1.0
 */
public abstract class AbstractShapeFile implements GestionShapeFile {
	
	@Override
	public void openShapeFile() throws IOException{
		File file = JFileDataStoreChooser.showOpenFile("shp", null);
		if(file == null){
			return;
		}
		
		FileDataStore store = FileDataStoreFinder.getDataStore(file);
		SimpleFeatureSource featuresource = store.getFeatureSource();
        MapContent map = new MapContent();
		
		Style style = SLD.createSimpleStyle(featuresource.getSchema());
		Layer layer = new FeatureLayer(featuresource, style);
		map.addLayer(layer);
		
		JMapFrame.showMap(map);
	}
	
	
	@Override
	public abstract void createNewShapeFile() throws SchemaException, IOException; 
	
}
