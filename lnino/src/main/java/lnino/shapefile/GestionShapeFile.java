package lnino.shapefile;

import java.io.IOException;

import org.geotools.feature.SchemaException;

import lnino.exception.PasException;

/**
 * Classe permettant l'ouverture et la création de shapefile
 * 
 * @author lnino
 * @version 1.0
 */
public interface GestionShapeFile {
	
	/**
	 * Cette méthode permet d'ouvrir un shapefile à partir de la librairie geotools
	 * @throws IOException
	 */
	public void openShapeFile() throws IOException;
	
	/**
	 * Cette méthode permet de créer un nouveau shapefile contenant les géométries simplifiées
	 * 		à partir de la librairie geotools 
	 * @throws SchemaException
	 * @throws IOException
	 * @throws PasException
	 */
	public void createNewShapeFile() throws SchemaException, IOException, PasException;

}
