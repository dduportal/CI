package lnino.generalisation;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.vividsolutions.jts.geom.Coordinate;

import lnino.exception.PasException;

public class GeneralisationPointsTest {

	@Test
	public void testGenUnMPoints() throws PasException {
		List<Coordinate> coords = new ArrayList<>();
		
		Coordinate c = new Coordinate(5,3);
		Coordinate c2 = new Coordinate(5,9);
		
		coords.add(c);
		coords.add(c2);
		
		GeneralisationPoints npoints = new GeneralisationPoints(10);
		
		assertEquals(npoints.genPoint(coords, 0, 1000, 0, 1000).get(0), new Coordinate(5,6));
	}
	
	@Test
	public void testGenPlsMPoints() throws PasException {
		List<Coordinate> coords = new ArrayList<>();
		
		Coordinate c = new Coordinate(5,3);
		Coordinate c2 = new Coordinate(5,9);
		Coordinate c3 = new Coordinate(9,6);
		Coordinate c4 = new Coordinate(4,1);
		
		coords.add(c);
		coords.add(c2);
		coords.add(c3);
		coords.add(c4);
		
		GeneralisationPoints npoints = new GeneralisationPoints(10);
		
		assertEquals(npoints.genPoint(coords, 0, 1000, 0, 1000).get(0), new Coordinate(5.75,4.75));
	}

	@Test
	public void testCalculMoyenneX() throws PasException {
		Coordinate c = new Coordinate(5,3);
		Coordinate c2 = new Coordinate(5,9);
		
		List<Coordinate> coords = new ArrayList<>();
		coords.add(c);
		coords.add(c2);
		
		GeneralisationPoints npoints = new GeneralisationPoints(100);
		
		assertTrue(npoints.calculMoyenneX(coords) == 5);
	}

	@Test
	public void testCalculMoyenneY() throws PasException {
		Coordinate c = new Coordinate(5,3);
		Coordinate c2 = new Coordinate(5,9);
		
		List<Coordinate> coords = new ArrayList<>();
		coords.add(c);
		coords.add(c2);
		
		GeneralisationPoints npoints = new GeneralisationPoints(100);
		
		assertTrue(npoints.calculMoyenneY(coords) == 6);
	}

	@Test
	public void testCreatePoint() throws PasException {
		GeneralisationPoints npoints = new GeneralisationPoints(5);
		
		Coordinate c = new Coordinate(1,1);
		
		assertEquals(npoints.createPoint(c).getGeometryType(), "Point");
	}
	

}
