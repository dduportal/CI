package lnino.generalisation;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;

public class GeneralisationPolygonsTest {

	@Test
	public void testGenePolygon() {
		GeneralisationPolygons poly = new GeneralisationPolygons();
		Coordinate c1 = new Coordinate(0, 0);
		Coordinate c2 = new Coordinate(0, 1);
		Coordinate c3 = new Coordinate(1, 1);
		Coordinate c4 = new Coordinate(1, 0);
		
		Coordinate[] coords = {c1, c2, c3, c4, c1};
		
		GeometryFactory geometryFactory = new GeometryFactory();
		Polygon p = geometryFactory.createPolygon(coords);
		
		Coordinate c1p = new Coordinate(0.5, 0);
		Coordinate c2p = new Coordinate(1, 0.5);
		Coordinate c3p = new Coordinate(0.7, 0.7);
		Coordinate c4p = new Coordinate(0.8, 0.9);
		Coordinate c5p = new Coordinate(0.6, 1);
		Coordinate c6p = new Coordinate(0, 0.5);
		
		Coordinate[] coordsp = {c1p, c2p, c3p, c4p, c5p, c6p, c1p};
		
		Polygon pp = geometryFactory.createPolygon(coordsp);
		
		assertEquals(p, poly.createPolygone(poly.genePolygon(pp)));
		
	}

	@Test
	public void testCreatePolygone() {
		GeneralisationPolygons poly = new GeneralisationPolygons();
		Coordinate c1 = new Coordinate(1, 5);
		Coordinate c2 = new Coordinate(4, 5);
		Coordinate c3 = new Coordinate(8, 3);
		
		Coordinate[] coords = {c1, c2, c3, c1};
		assertEquals(poly.createPolygone(coords).getGeometryType(), "Polygon");
	}

}
